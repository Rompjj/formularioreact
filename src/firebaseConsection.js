import firebase from 'firebase/app';
import 'firebase/firestore';

let firebaseConfig = {
    apiKey: "AIzaSyDJZgYBGK-nUOsGJ0N7DJaie28rBSIcmiY",
    authDomain: "estagio-ed613.firebaseapp.com",
    projectId: "estagio-ed613",
    storageBucket: "estagio-ed613.appspot.com",
    messagingSenderId: "538969125307",
    appId: "1:538969125307:web:e2d174984a975583fd949e",
    measurementId: "G-8JJXG8L6PE"
  };
  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

export default firebase;