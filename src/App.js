import { useState } from 'react';
import './style.css';

import firebase from './firebaseConsection';

function App() {
  const [idPost, setIdPost] = useState('');
  const [nome, setNome] = useState('');
  const [telefone, setTelefone] = useState('');
  const [cidade, setCidade] = useState('');

  async function cadastrar() {
    if (nome !== '' && telefone !== '' && cidade !== '') {
      await firebase.firestore().collection('cadastros')
        .add({
          nome: nome,
          telefone: telefone,
          cidade: cidade,
        })
        .then(() => {
          console.log('Dados cadastrados com sucesso!');
          setTelefone('');
          setNome('');
          setCidade('');
          alert('Cadastro realizado com sucesso!');
        })
        .catch((error) => {
          alert('GEROU ALGUM ERRO: ' + error);
          console.log('GEROU ALGUM ERRO: ' + error);
        })
    } else {
      alert('Preencha os campos em branco.');
    }
  }

  async function buscar() {
    if (idPost !== '') {
      await firebase.firestore().collection('cadastros')
        .doc(idPost)
        .get()
        .then((snapshot) => {
          setNome(snapshot.data().nome);
          setTelefone(snapshot.data().telefone);
          setCidade(snapshot.data().cidade);
          console.log('Cadastro encontrado.');
        })
        .catch(() => {
          alert('Cadastro não encontrado.');
          console.log('BUSCA PELO CADASTRO FALHOU!');
        })
    } else {
      alert("Insira o numero de cadastro para sua busca.");
    }
  }

  async function editar() {
    await firebase.firestore().collection('cadastros')
      .doc(idPost)
      .update({
        nome: nome,
        telefone: telefone,
        cidade: cidade,
      })
      .then(() => {
        alert('Cadastro atualizados com sucesso!');
        console.log('Cadastro atualizados com sucesso!');
        setIdPost('');
        setTelefone('');
        setNome('');
        setCidade('');
      })
      .catch(() => {
        alert('Cadastro não foi atualizado!');
        console.log('ERRO AO ATUALIZAR');
      });
  }

  async function excluir() {
    await firebase.firestore().collection('cadastros')
      .doc(idPost)
      .delete()
      .then(() => {
        alert('Cadastro: ' + nome + ' foi excluido com sucesso!');
        console.log('Cadastro: ' + nome + ' foi excluido com sucesso!');
        setIdPost('');
        setTelefone('');
        setNome('');
        setCidade('');
      })
  }


  return (
    <div>
      <header id="title">
        <h1>Cândido Empresarial - cadastro</h1>
      </header>

    <fieldset>
      <nav className="containerNav">
        <button onClick={cadastrar}>Cadastrar</button>
        <button onClick={buscar}>Buscar</button>
        <button onClick={editar}>Editar</button>
        <button onClick={excluir}>Exlcuir</button> <br />
      </nav>

      <section className="containerFild">
        <label>Cadastro: </label>
        <input type="text" value={idPost} onChange={(e) => setIdPost(e.target.value)} placeholder="Register"/>

        <label>Nome: </label>
        <input type="text" value={nome} onChange={(e) => setNome(e.target.value)} placeholder="Name"/>

        <label>Telefone: </label>
        <input type="text" value={telefone} onChange={(e) => setTelefone(e.target.value)} placeholder="Phone"/>

        <label>Cidade: </label>
        <select onChange={(e) => setCidade(e.target.value)}>
          <option value='Cidade'>City</option>
          <option value='Uberaba'>Uberaba</option>
          <option value='São Paulo'>São Paulo</option>
          <option value='Rio de janeiro'>Rio de janeiro</option>
          <option value='Bertioga'>Bertioga</option>
          <option value='Araraquara'>Araraquara</option>
        </select>
      </section>
    </fieldset>

    </div>
  );
}

export default App;
